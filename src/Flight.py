class Flight(object):
  def __init__(self, flightNumber, departureTime, arrivalTime, businessFare, leisureFare, capacity):
    self.flightNumber = flightNumber
    self.departureTime = departureTime
    self.arrivalTime = arrivalTime
    self.businessFare = businessFare
    self.leisureFare = leisureFare
    self.capacity = capacity
    
  def resetForNewCycle(self):
    self.ticketsSoldBusiness = 0
    self.ticketsSoldLeisure = 0
    
    self.wantedToBuyBusiness = 0
    self.wantedToBuyLeisure = 0
    
  def isFull(self):
    return self.capacity == self.ticketsSold()

  def availablePlaces(self):
    return self.capacity - self.ticketsSold()

  def reserveTicket(self, isBusiness, day):
    if isBusiness:
      self.ticketsSoldBusiness += 1
      # print("buis" + str(self.ticketsSoldBusiness))
    else:
      self.ticketsSoldLeisure += 1
      # print("leis" + str(self.ticketsSoldLeisure))

  def ticketsSold(self):
    return self.ticketsSoldBusiness + self.ticketsSoldLeisure
    
  def getFare(self, isBusiness):
    return self.businessFare if isBusiness else self.leisureFare
  
  def copy(self):
    newFlight = Flight(self.flightNumber, self.departureTime, self.arrivalTime, self.businessFare, self.leisureFare, self.capacity)
    newFlight.ticketsSoldBusiness = self.ticketsSoldBusiness
    newFlight.ticketsSoldLeisure = self.ticketsSoldLeisure
    newFlight.wantedToBuyBusiness = self.wantedToBuyBusiness
    newFlight.wantedToBuyLeisure = self.wantedToBuyLeisure
    return newFlight
