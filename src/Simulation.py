import Params
import numpy as np
import random
from Passenger import Passenger
from Network import Network

class Simulation(object):
  def __init__(self, airlines, adjustPrices=False, verbose=False):
    self.airlines = airlines
    self.flights = [(airline, flight) for airline in airlines for flight in airline.flights]
    leisureCount = int(random.normalvariate(Params.DemandMean["l"], Params.DemandSigma["l"]))
    businessCount = int(random.normalvariate(Params.DemandMean["b"], Params.DemandSigma["b"]))
    self.businessPassengers = [Passenger(isBusiness=True, passengerId=i) for i in xrange(businessCount)]
    self.leisurePassengers = [Passenger(isBusiness=False, passengerId=(i + businessCount)) for i in xrange(leisureCount)]
    self.businessDemand = Params.demand("b")
    self.leisureDemand = Params.demand("l")
    self.adjustPrices = adjustPrices
    self.social_network = Network(self.businessPassengers + self.leisurePassengers)
    self.verbose = verbose

  def todaysDemand(self, day, isBusiness):
    return (self.businessDemand if isBusiness else self.leisureDemand)[day]

  def simulate(self, leaderMarketingStrategy, followerFirstMarketingStrategy, followerSecondMarketingStrategy, startDay = 0):
    resultLogs = {"ticketsSold": [], "fares": {}}
    for (airline, flight) in self.flights:
      resultLogs["fares"][str(flight.flightNumber) + ":b"] = []
      resultLogs["fares"][str(flight.flightNumber) + ":l"] = []

    for passenger in self.businessPassengers:
      passenger.resetForNewSimulation()
    for passenger in self.leisurePassengers:
      passenger.resetForNewSimulation()

    leader, follower = self.airlines
    leader.initialize(leaderMarketingStrategy)
    follower.initialize(followerFirstMarketingStrategy)
    
    for cycle in xrange(0, Params.CyclesNumber):
      for airline in self.airlines:
        airline.newCycle()
      self.businessDemand = Params.demand("b")
      self.leisureDemand = Params.demand("l")

      for day in xrange(startDay, Params.TicketingTime):

        if self.verbose:
          print "Simulating day " + str(day) + "..."
      
        if day + cycle * Params.TicketingTime == Params.FollowerMarketingStrategyDelay:
          follower.setMarketingStrategy(followerSecondMarketingStrategy)
        
        if self.adjustPrices:
          airlineToCheck = day % len(self.airlines)
          competition = self.airlines[:]
          del competition[airlineToCheck]
          self.airlines[airlineToCheck].adjustPrice(day, leaderMarketingStrategy, followerFirstMarketingStrategy, followerSecondMarketingStrategy, competition)
          for (airline, flight) in self.flights:
            resultLogs["fares"][str(flight.flightNumber) + ":b"].append(flight.businessFare)
            resultLogs["fares"][str(flight.flightNumber) + ":l"].append(flight.leisureFare)

        todaysBusinessDemand = self.todaysDemand(day, True)
        todaysLeisureDemand = self.todaysDemand(day, False)
        
        todaysPassengers = random.sample(self.businessPassengers, todaysBusinessDemand)
        todaysPassengers.extend(random.sample(self.leisurePassengers, todaysLeisureDemand))
        random.shuffle(todaysPassengers)

        airlineA = self.airlines[0]
        airlineB = self.airlines[1]

        # Pasazer wybiera lot i kupuje bilet.
        for passenger in todaysPassengers:
          flightsWithUtility = sorted([(-passenger.utilityFunction(offer), airline, offer)
                                for airline in self.airlines
                                for offer in airline.getOffers(passenger.isBusiness)])
          for (_, airline, offer) in flightsWithUtility:
            if airline.buyTicket(offer, passenger.isBusiness, day):
              # Kupil bilet i polecial, uaktualniamy satisfaction:
              passenger.updateSatisfaction(airline, offer, airlineA if airline.name == airlineB.name else airlineB,
                  self.social_network)
              # zapisujemy to w logach
              resultLogs["ticketsSold"].append((airline.name, offer["flight"].flightNumber, cycle, day, passenger.isBusiness, offer["fare"], offer["quality"]))
              break
              
      for airline in self.airlines:
        airline.endCycle()
    
    resultLogs["revenue"] = [airline.revenue for airline in self.airlines]
    return resultLogs
