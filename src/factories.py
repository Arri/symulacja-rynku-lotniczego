from MarketingStrategy import *
from Flight import Flight
from Airline import Airline

from Params import capacities

NeutralStrategy = MarketingStrategy(0, 0)

def createAirlines(strategyA, strategyB):
  flightsA = [Flight(101,  7*60, 12*60, 150, 60, capacities[0]),
              Flight(102, 10*60, 15*60, 150, 60, capacities[0])]
  flightsB = [Flight(201,  8*60, 13*60, 170, 60, capacities[1]),
              Flight(202,  9*60, 14*60, 150, 60, capacities[1])]

  airlines = [Airline("A", flightsA, strategyA),
              Airline("B", flightsB, strategyB)]

  return airlines
