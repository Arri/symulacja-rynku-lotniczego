from scipy.stats import norm
from GreedyStrategy import GreedyStrategy
from Simulation import Simulation
from factories import createAirlines, NeutralStrategy
import math

def NewsBoyCapacity(mDist, Underage, Overrage):
    (mean, Dist) = mDist
    q = float(Underage)/(Underage + Overrage)
#    print mean, q, Dist.ppf(q)
    return Dist.ppf(q)
    
def EMSRLimits(j, DistList, fares, Cap):
    capacities = [NewsBoyCapacity(DistList[i], fares[i] - fares[j], fares[j]) for i in xrange(0,j)]
    return min(math.ceil(sum(capacities)), Cap)

def EMSRLimitsList(j, DistList, fares, Cap):
    return [EMSRLimits(i, DistList, fares, Cap) for i in xrange(0, j)]
    
class EMSRLimitsStrategy(object):
  def __init__(self):
    self.limits = {}
    
  def calculateEMSRLimit(self, flight):
    simulation = Simulation(createAirlines(GreedyStrategy(), GreedyStrategy()))
    simulation.simulate(NeutralStrategy, NeutralStrategy, NeutralStrategy)
    means = dict((flight.flightNumber, [flight.wantedToBuyBusiness, flight.wantedToBuyLeisure]) for (_, flight) in simulation.flights)[flight.flightNumber]
    DistList = [(mean, norm(mean, mean/10.0)) for mean in means]
    limits = EMSRLimitsList(2, DistList, [flight.businessFare, flight.leisureFare], flight.capacity)
#    print flight.flightNumber, limits
    return limits
    
  def canBuy(self, flight, isBusiness):
    # Calculate the limit if it is not already memorized.
    if flight not in self.limits:
      self.limits[flight] = self.calculateEMSRLimit(flight)
    reservedUpperClass = self.limits[flight][0 if isBusiness else 1]
    soldUpperClass = 0 if isBusiness else flight.ticketsSoldBusiness
    seatsLeft = flight.capacity - flight.ticketsSold() - (reservedUpperClass - soldUpperClass)
#    print flight.flightNumber, isBusiness, seatsLeft
    return seatsLeft > 0
