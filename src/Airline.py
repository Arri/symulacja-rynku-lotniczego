from Simulation import Simulation
from Params import PriceDelta

class Airline(object):
  def __init__(self, name, flights, strategy):
    self.name = name
    self.flights = flights
    self.strategy = strategy
    
  def initialize(self, marketingStrategy):
    self.revenue = 0
    self.setMarketingStrategy(marketingStrategy)
    
  def setMarketingStrategy(self, marketingStrategy):
    self.marketingStrategy = marketingStrategy

  def hasQualityAds(self):
    return self.marketingStrategy.getQualityAds() == 1

  def newCycle(self):
    for flight in self.flights:
      flight.resetForNewCycle()
      
  def endCycle(self):
    costs = 0
    for flight in self.flights:
      qualityCost = self.marketingStrategy.getQualitySpending()
      qualityAdsCost = self.marketingStrategy.getQualityAdsSpending()
      costs += qualityCost + qualityAdsCost
    self.revenue -= costs

  def canBuy(self, flight, isBusiness):
    return self.strategy.canBuy(flight, isBusiness)
    
  def buyTicket(self, offer, isBusiness, day):
    flight = offer["flight"]
    if isBusiness:
      flight.wantedToBuyBusiness += 1
    else:
      flight.wantedToBuyLeisure += 1
    if self.canBuy(flight, isBusiness):
      self.revenue += offer["fare"]
      flight.reserveTicket(isBusiness, day)
      return True
    else:
      return False
      
  def makeOffer(self, flight, isBusiness):
    return {"fare": flight.getFare(isBusiness),
            "arrivalTime": flight.arrivalTime,
            "departureTime": flight.departureTime,
            "quality": self.marketingStrategy.getQuality(),
            "qualityAds": self.marketingStrategy.getQualityAds(),
            "airline": self.name,
            "flight": flight}
            
  def getOffers(self, isBusiness):
    return [self.makeOffer(flight, isBusiness) for flight in self.flights]
  
  def adjustPrice(self, day, leaderMarketingStrategy, followerFirstMarketingStrategy, followerSecondMarketingStrategy, competition):
    selfCopy = self.copy()
    withIncreased = self.copy().changePrices()
    withDecreased = self.copy().changePrices(-1)
    basePredictedRevenue = predictRevenue(day, leaderMarketingStrategy, followerFirstMarketingStrategy, followerSecondMarketingStrategy, competition, selfCopy)
    increasedPredictedRevenue = predictRevenue(day, leaderMarketingStrategy, followerFirstMarketingStrategy, followerSecondMarketingStrategy, competition, withIncreased)
    decreasedPredictedRevenue = predictRevenue(day, leaderMarketingStrategy, followerFirstMarketingStrategy, followerSecondMarketingStrategy, competition, withDecreased)
    maxRevenue = max([basePredictedRevenue, increasedPredictedRevenue, decreasedPredictedRevenue])
    if maxRevenue == increasedPredictedRevenue:
      # print "Increasing fares"
      map(mergeFlightPrice, zip(self.flights, withIncreased.flights))
    elif maxRevenue == decreasedPredictedRevenue:
      # print "decreasing fares"
      map(mergeFlightPrice, zip(self.flights, withDecreased.flights))
    else:
      # print "Not changing fares"
      pass
      
  def getLeisurePrice(self):
    prices = 0.0
    for flight in self.flights:
      prices += flight.leisureFare
    return prices / float(len(self.flights))
     
  def getBusinessPrice(self):
    prices = 0.0
    for flight in self.flights:
      prices += flight.businessFare
    return prices / float(len(self.flights))
    
  def copy(self):
    newAirline = Airline(self.name, map(lambda f: f.copy(), self.flights), self.strategy)
    newAirline.revenue = self.revenue
    newAirline.marketingStrategy = self.marketingStrategy
    return newAirline
  
  def changePrices(self, priceDeltaFactor = 1):
    map(changeFlightPrice(priceDeltaFactor), self.flights)
    return self
  

def changeFlightPrice(priceDeltaFactor):
  def changeFlight(flight):
    flight.businessFare = flight.businessFare + priceDeltaFactor * PriceDelta["b"] * flight.businessFare
    flight.leisureFare = flight.leisureFare + priceDeltaFactor * PriceDelta["l"] * flight.leisureFare
    return flight
  return changeFlight


def predictRevenue(day, leaderMarketingStrategy, followerFirstMarketingStrategy, followerSecondMarketingStrategy, competition, main):
  comp = competition[:]
  comp.append(main)
  simulation = Simulation(comp)
  simulation.simulate(leaderMarketingStrategy, followerFirstMarketingStrategy, followerSecondMarketingStrategy, day)
  return main.revenue

def mergeFlightPrice(flightPair):
  flightPair[0].businessFare = flightPair[1].businessFare
  flightPair[0].leisureFare = flightPair[1].leisureFare

