from math import sqrt
import Params

"""

Dla kazdego z parametrow:
- reklamy jakosci: 0 lub 1
- jakosc: -1, 0 lub 1
"""

class MarketingStrategy(object):
  def __init__(self, qualityAds, quality):
    self.qualityAds = qualityAds
    self.quality = quality
  
  def __str__(self):
    return "%d, %d" % (self.qualityAds, self.quality)

  def getQuality(self):
    return self.quality

  def getQualityAds(self):
    return self.qualityAds

  def getQualitySpending(self):
    return float(Params.QUALITY_COST * self.quality)

  def getQualityAdsSpending(self):
    return float(Params.QUALITY_ADS_COST * self.qualityAds)
