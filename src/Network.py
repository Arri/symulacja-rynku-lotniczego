import random
import math
from sets import Set

class Network(object):

  def __init__(self, objects):
    size = len(objects)
    self.objects = objects
    self.connections = [[] for i in xrange(size)]
    edgesNumber = 0
    for i in xrange(size):
      self.addNode(i, edgesNumber)
      edgesNumber += 2 * len(self.connections[i])

  def addNode(self, node, edgesNumber):
    connected = False
    for i in xrange(node):
      chance = 1 if edgesNumber == 0 else float(len(self.connections[i])) / float(edgesNumber)
      if chance > random.random:
        self.connections[i].append(node)
        self.connections[node].append(i)
        connected = True
    if not connected and node > 0:
      i = random.randint(0, node - 1)
      self.connections[i].append(node)
      self.connections[node].append(i)

  def getNext(self, node):
    return Set(self.connections[node])

  def getNextLevels(self, node, levels):
    levels += 1
    nextLevel = [Set() for i in xrange(levels)]
    nextLevel[0].add(node)
    allAdded = Set([node])
    for i in range(1, levels):
      for j in nextLevel[i - 1]:
        nextLevel[i] |= self.getNext(j)
      nextLevel[i] -= allAdded
      allAdded |= nextLevel[i]
    return [(i, [self.objects[j] for j in nextLevel[i]]) for i in range(1, levels)]

