import json
from math import exp
from scipy import integrate
from numpy import random, cumsum, mean, std, asscalar
import matplotlib.pyplot as plt

DEMAND_CYCLES = 20

capacities = [120, 150]

intensityBusiness = 0.1
intensityLeisure = 0.5

MARKETS = ["b", "l"]

LABELS = { "b": "Business", "l": "Leisure" }

intensity = { "b": intensityBusiness, "l": intensityLeisure }

ShapeBusiness = 0.035
ShapeLeisure =  0.01

shape = { "b": ShapeBusiness, "l": ShapeLeisure }

TicketingTime = 90

CyclesNumber = 1
FollowerMarketingStrategyDelay = CyclesNumber * TicketingTime / 2

DemandMean = { "b": 0, "l": 0 }
DemandSigma = { "b": 0, "l": 0 }

PriceDelta = { "b": 0.1, "l": 0.1 }

QUALITY_COST = 1000
QUALITY_ADS_COST = 2000

def bookingIntensity(market, time):
  return intensity[market] * exp(-1 * shape[market] * (TicketingTime - time))

def averageBookings(market, day):
  return integrate.quad(lambda x: bookingIntensity(market, x), 1, day)[0]

def numberOfBookings(market, day):
  return random.poisson(averageBookings(market, day))

def demand(market):
  return [numberOfBookings(market, day) for day in xrange(1, TicketingTime + 1)]

def totalDemand(market, opt_demand = None):
  if opt_demand is None:
    return list(cumsum(demand(market)))
  return list(cumsum(opt_demand))

def calculateDemand():
  for market in MARKETS:
    calculateMarketDemand(market)
  
  plt.figure(1, figsize=(10,10))
  for market in MARKETS:
    plt.plot(xrange(0, TicketingTime), [bookingIntensity(market, x) for x in xrange(0, TicketingTime)], label="%s intensity" % LABELS[market])
  plt.legend(loc=2)
  plt.savefig('bookingIntensity.png')
  plt.clf()
  
  plt.figure(1, figsize=(10,10))
  for market in MARKETS:
    plt.plot(xrange(0, TicketingTime), [averageBookings(market, x) for x in xrange(0, TicketingTime)], label="Average %s Bookings" % LABELS[market])
  plt.legend(loc=2)
  plt.savefig('averageBookings.png')
  plt.clf()
  
  plt.figure(1, figsize=(10,10))
  for market in MARKETS:
    marketDemand = demand(market)
    plt.plot(xrange(0, TicketingTime), marketDemand, label="%s day demand" % LABELS[market])
    plt.plot(xrange(0, TicketingTime), totalDemand(market, marketDemand), label="%s total demand" % LABELS[market])
  plt.legend(loc=2)
  plt.savefig('demand.png')
  plt.clf()

def calculateMarketDemand(market):
  marketTotalDemands = [totalDemand(market)[TicketingTime - 1] for x in xrange(DEMAND_CYCLES)]
  DemandMean[market] = mean(marketTotalDemands)
  DemandSigma[market] = std(marketTotalDemands)

def loadJSONParams(filePath):
  json_data=open(filePath)
  data = json.load(json_data)
  json_data.close()
  CyclesNumber = float(data["CyclesNumber"])
  TicketingTime = float(data["TicketingTime"])
  ShapeBusiness = float(data["ShapeBusiness"])
  ShapeLeisure = float(data["ShapeLeisure"])
  intensityBusiness = float(data["intensityBusiness"])
  intensityLeisure = float(data["intensityLeisure"])
  capacities[0] = float(data["airline1Capacity"])
  capacities[1] = float(data["airline2Capacity"])
  intensity = { "b": intensityBusiness, "l": intensityLeisure }
  shape = { "b": ShapeBusiness, "l": ShapeLeisure }
  FollowerMarketingStrategyDelay = CyclesNumber * TicketingTime / 2
  return data
