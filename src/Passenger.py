import random
import math
import datetime

class Passenger(object):

  def __init__(self, isBusiness, passengerId):
    self.id = passengerId
    self.isBusiness = isBusiness
    sigmaDiv = math.sqrt(1546 if isBusiness else 3914)
    self.arriveEarlyWeight = random.normalvariate(mu = -0.161 if isBusiness else -0.092, sigma = (-2.518 if isBusiness else -2.622) / sigmaDiv)
    self.arriveLateWeight = random.normalvariate(mu = -0.471 if isBusiness else -0.449, sigma = (-3.401 if isBusiness else -4.743) / sigmaDiv)

    self.departEarlyWeight = random.normalvariate(mu = -0.803 if isBusiness else -0.364, sigma = (-3.901 if isBusiness else -2.620) / sigmaDiv)
    self.departLateWeight = random.normalvariate(mu = -0.522 if isBusiness else -0.094, sigma = (-1.781 if isBusiness else -3.681) / sigmaDiv)
    self.durationWeight = random.normalvariate(mu = -1.227 if isBusiness else -0.54, sigma = (0.091 if isBusiness else 0.101) / sigmaDiv)
    self.fare = random.normalvariate(mu = -4.216 if isBusiness else -4.549, sigma = (0.198 if isBusiness else 0.245) / sigmaDiv)
    self.lowerWindow = 0.75 if isBusiness else 1.5
    self.upperWindow = 0.5 if isBusiness else 1.25
    self.idealDepartureTime = random.normalvariate(mu = 8, sigma = 1.5) * 0.7 + random.normalvariate(mu = 18, sigma = 1.5) * 0.3
    self.resetForNewSimulation()
    
  def resetForNewSimulation(self):
    self.satisfaction = {"A": 1.0, "B": 1.0}

  def utilityFunction(self, offer):
    result = self.fare * math.log(offer["fare"])
    result += self.durationWeight * (offer["arrivalTime"] - offer["departureTime"]) / 60.0
    
    timeDiff = self.idealDepartureTime - (offer["departureTime"] / 60.0)
   
    if timeDiff > 0:
        windowDiff = timeDiff - self.lowerWindow
        if windowDiff > 0:
            result += self.departEarlyWeight * (windowDiff + 1)
    if timeDiff < 0:
        windowDiff = (-timeDiff) - self.upperWindow
        if windowDiff > 0:
            result += self.departLateWeight * (windowDiff + 1)

    # Tutaj dzielimy zamiast mnozyc bo tak dokladnie to to jest DISUTILITY
    result /= self.satisfaction[offer["airline"]]
    result /= (1.0 + 2 * offer["qualityAds"])
    
    return result

  def updateSatisfaction(self, airline, offer, competitor, network):
    quality = offer["quality"]
    price = offer["fare"]
    competitorPrice = competitor.getBusinessPrice() if self.isBusiness else competitor.getLeisurePrice()
    expectedQuality = 0
    if airline.hasQualityAds():
      expectedQuality += 1
    if price < (competitorPrice * 0.9):
      expectedQuality -= 1
    y = quality - expectedQuality + 2
    fy = [-0.06, -0.03, 0.01, 0.025, 0.05][y]
    if fy > 0.0:
      if price > competitorPrice:
        x = 2
      elif price < competitorPrice:
        x = 4
      else:
        x = 1
    else:
      if price > competitorPrice:
        x = 4
      elif price < competitorPrice:
        x = 2
      else:
        x = 1
    diff = (x * fy)
    self.satisfaction[offer["airline"]] *= (1.0 + diff)
    for (level, friends) in network.getNextLevels(self.id, 3):
      for friend in friends:
        friend.satisfaction[offer["airline"]] *= (1.0 + (diff * pow(0.25, level)))
