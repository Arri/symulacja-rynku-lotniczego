#!/usr/bin/env python
from Flight import Flight
from Airline import Airline
from GreedyStrategy import GreedyStrategy
from EMSRLimitsStrategy import EMSRLimitsStrategy
from Simulation import Simulation
from Params import loadJSONParams, calculateDemand
from utils import *
from MarketingStrategy import *
from factories import createAirlines, NeutralStrategy
import sys
import matplotlib.pyplot as plt

loadJSONParams("../config.json")
calculateDemand()
airlines = createAirlines(EMSRLimitsStrategy(), EMSRLimitsStrategy())
simulation = Simulation(airlines, False, True)

followerInitialMarketingStrategy = MarketingStrategy(1, 1)#NeutralStrategy # follower zaczyna z jakas strategia

# allStrategies = [MarketingStrategy(price, ads, quality)
#                   for price in [-1, 0, 1]
#                   for ads in [-1, 0, 1]
#                   for quality in [-1, 0, 1]]

# no marketing strategy to see how pricing change work
allStrategies = [NeutralStrategy]

""" OKROJONA WERSJA ZEBY DO TESTOW BYLO SZYBCIEJ """
"""allStrategies = [MarketingStrategy(price, ads, quality)
                  for price in [0, 1]
                  for ads in [0]
                  for quality in [0, 1]]
"""

# print "Simulating..."

possibleOutcomesForLeader = []
for leaderMarketingStrategy in allStrategies:
  possibleOutcomesForFollower = []
  for followerModifiedMarketingStrategy in [followerInitialMarketingStrategy]:
    # print ".",
    sys.stdout.flush()

    result = simulation.simulate(leaderMarketingStrategy, followerInitialMarketingStrategy, followerModifiedMarketingStrategy)
    possibleOutcomesForFollower.append((result["revenue"][1], followerModifiedMarketingStrategy, result))
  (_, followerChoice, bestOutcomeForFollower) = max(possibleOutcomesForFollower)
  leaderRevenue = bestOutcomeForFollower["revenue"][0]
  print "LEADER plays %s ==> FOLLOWER plays %s\t(leader earns %d)" % (str(leaderMarketingStrategy), str(followerChoice), leaderRevenue)
  possibleOutcomesForLeader.append((leaderRevenue, leaderMarketingStrategy, followerModifiedMarketingStrategy, bestOutcomeForFollower))

(_, leaderChoice, followerChoice, finalResult) = max(possibleOutcomesForLeader)
print "LEADER chooses %s" % str(leaderChoice)


# tutaj jest prezentacja scenariusza ktory wg nas sie wydarzy, bo wybral go lider minimaksem
# mozna by przedstawic jakies porownanie roznych mozliwych scenariuszy

for airline, revenue in zip(airlines, finalResult["revenue"]):
  print "Airline %s revenue = %d" % (airline.name, revenue)


ticketsSoldByAirline = [
  [
    [(airlineName, flightNumber, cycle, day, isBusiness, fare, quality) for (airlineName, flightNumber, cycle, day, isBusiness, fare, quality) in finalResult["ticketsSold"] if flightNumber == flight.flightNumber]
    for flight in airline.flights]
  for airline in airlines]

  
for airlineNumber in xrange(0, 2):
  for flightNumber in xrange(0, 2):
    print "tickets sold by Airline %s for flight %d: %d" % (airlines[airlineNumber].name, flightNumber, len(ticketsSoldByAirline[airlineNumber][flightNumber]))


# wykres ilosc sprzedanych/dzien
# klasa biznes [A0, A1, B0, B1]
bTicketsSoldByAirlineByDay = [[0]*90, [0]*90, [0]*90, [0]*90]
# klasa ekonomiczna [A0, A1, B0, B1]
lTicketsSoldByAirlineByDay = [[0]*90, [0]*90, [0]*90, [0]*90]

for airlineNumber in xrange(0,2):
    for flightNumber in xrange(0,2):
        for (aN, fN, cycle, day, isBusiness, fare, quality) in ticketsSoldByAirline[airlineNumber][flightNumber]:
            if isBusiness:
                bTicketsSoldByAirlineByDay[2*airlineNumber + flightNumber][day] += 1
            else:
                lTicketsSoldByAirlineByDay[2*airlineNumber + flightNumber][day] += 1
    

#print bTicketsSoldByAirlineByDay, lTicketsSoldByAirlineByDay

#plt.figure(1, figsize=(10,10))
#for legend, fares in finalResult["fares"].iteritems():
#  plt.plot(range(0,90), list(fares), label=legend)
#plt.legend(loc=2)
#plt.savefig('fares.png')
#plt.clf()

plt.figure(1, figsize=(6,6))
plt.plot(range(0,90), list(partial_sums(bTicketsSoldByAirlineByDay[0])), label="A: Flight 1 business")
plt.plot(range(0,90), list(partial_sums(lTicketsSoldByAirlineByDay[0])), label="A: Flight 1 leisure")
plt.plot(range(0,90), list(partial_sums(bTicketsSoldByAirlineByDay[1])), label="A: Flight 2 business")
plt.plot(range(0,90), list(partial_sums(lTicketsSoldByAirlineByDay[1])), label="A: Flight 2 leisure")
plt.legend(loc=2)
plt.savefig('plot_A.png')
plt.clf()

plt.figure(1, figsize=(6,6))
plt.plot(range(0,90), list(partial_sums(bTicketsSoldByAirlineByDay[2])), label="B: Flight 1 business")
plt.plot(range(0,90), list(partial_sums(lTicketsSoldByAirlineByDay[2])), label="B: Flight 1 leisure")
plt.plot(range(0,90), list(partial_sums(bTicketsSoldByAirlineByDay[3])), label="B: Flight 2 business")
plt.plot(range(0,90), list(partial_sums(lTicketsSoldByAirlineByDay[3])), label="B: Flight 2 leisure")
plt.legend(loc=2)
plt.savefig('plot_B.png')
plt.clf()
