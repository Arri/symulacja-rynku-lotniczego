class GreedyStrategy(object):
  def canBuy(self, flight, isBusiness):
    return not flight.isFull()
